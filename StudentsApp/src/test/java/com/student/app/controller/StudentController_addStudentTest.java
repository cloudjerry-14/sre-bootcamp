package com.student.app.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.student.app.model.Student;
import com.student.app.serviceimpl.StudentsServiceImpl;

@WebMvcTest(StudentsController.class)
class StudentController_addStudentTest {
	
	private static final String END_POINT_PATH = "/api/v1/student";
	 
    @Autowired 
    private MockMvc mockMvc;
    
    @Autowired 
    private ObjectMapper objectMapper;
    
    @MockBean
    private StudentsServiceImpl studentServiceImpl;
    
    @Test
    public void studentIsCreated() throws Exception {
    	Student student=new Student();
    	student.setFirstName("Tony");
    	student.setLastName("Stark");
    	student.setAge(23);
    	student.setGrade('A');
  
    	String requestBody = objectMapper.writeValueAsString(student);   	 
        mockMvc.perform(post(END_POINT_PATH).contentType("application/json")
                .content(requestBody))
                .andExpect(status().isCreated())
                .andDo(print());
    }
    
    @Test
    public void studentIsNotCreated() throws Exception {
    	Student student=new Student();
    	student.setFirstName("");
    	String requestBody = objectMapper.writeValueAsString(student);
    	 
        mockMvc.perform(post(END_POINT_PATH).contentType("application/json")
                .content(requestBody))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

}
