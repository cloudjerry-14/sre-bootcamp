package com.student.app.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.student.app.model.Student;
import com.student.app.serviceimpl.StudentsServiceImpl;

@WebMvcTest(StudentsController.class)
class StudentController_addAllStudentTest {

	private static final String END_POINT_PATH = "/api/v1/students";
	 
    @Autowired 
    private MockMvc mockMvc;
    
    @Autowired 
    private ObjectMapper objectMapper;
    
    @MockBean
    private StudentsServiceImpl studentServiceImpl;
    
    @Test
    public void studentsAreSaved() throws Exception {
    	List<Student> sL=new ArrayList<Student>();
    	Student student1=new Student();
    	student1.setFirstName("Tony");
    	student1.setLastName("Stark");
    	student1.setAge(23);
    	student1.setGrade('A');
  
    	Student student2=new Student();
    	student2.setFirstName("Jony");
    	student2.setLastName("Depp");
    	student2.setAge(53);
    	student2.setGrade('B');
    	
    	sL.add(student1);
    	sL.add(student2);
    	
    	String requestBody = objectMapper.writeValueAsString(sL);   	 
        mockMvc.perform(post(END_POINT_PATH).contentType("application/json")
                .content(requestBody))
                .andExpect(status().isCreated())
                .andDo(print());
    }
    
    @Test
    public void studentsAreNotSaved() throws Exception {
    	List<Student> sL=new ArrayList<Student>();
    	Student student1=new Student();
    	student1.setFirstName("");
    	sL.add(student1);
    	String requestBody = objectMapper.writeValueAsString(sL);
    	 
        mockMvc.perform(post(END_POINT_PATH).contentType("application/json")
                .content(requestBody))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

}
