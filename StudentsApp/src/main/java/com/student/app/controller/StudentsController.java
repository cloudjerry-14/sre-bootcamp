package com.student.app.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.student.app.exception.StudentNotFoundException;
import com.student.app.model.Student;
import com.student.app.serviceimpl.StudentsServiceImpl;

import jakarta.validation.Valid;

@RestController
public class StudentsController {
	
	private static final Logger logger = LoggerFactory.getLogger(StudentsController.class);
	
	@Value("${spring.application.name}")
    private String applicationName;
	
	@Autowired
	StudentsServiceImpl studentService;
	
	@GetMapping("/api/v1/students")
	public List<Student> getAllStudent(){	
		List<Student> students=new ArrayList<Student>();
		students=studentService.getAllStudents();
		return students;
	}
	
	@GetMapping("/api/v1/students/{studentId}")
	public ResponseEntity<Optional<Student>> getStudent(@Valid @PathVariable Long studentId) {
		Optional<Student> s = java.util.Optional.empty();
		try {
			s = studentService.getStudentById(studentId);
		} catch (StudentNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(s);
	}
	
	@PostMapping("/api/v1/student")
	public ResponseEntity<String> addStudent(@Valid @RequestBody Student student) {
		studentService.addStudent(student);
		logger.info(applicationName + " - Details saved successfully for student with Id : " + student.getStudentId());
		return new ResponseEntity<>("Student saved successfully with Id : " + student.getStudentId() , HttpStatus.CREATED);
	}
	
	@PostMapping("/api/v1/students")
	public ResponseEntity<String> addAllStudent(@Valid @RequestBody List<Student> student) {
		studentService.addAllStudent(student);
		logger.info(applicationName + " - List of student saved successfully!");
		return new ResponseEntity<>("All Students saved successfully!", HttpStatus.CREATED);
	}
	
	@DeleteMapping("/api/v1/students/{studentId}")
	public ResponseEntity<String> deleteStudent(@Valid @PathVariable Long studentId) {
		try {
			studentService.deleteStudentById(studentId);
		} catch (StudentNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info(applicationName + " - Student with id : " + studentId + " is deleted");
		return ResponseEntity.ok("The student with id : "+ studentId + " is deleted");
	}
	
	@DeleteMapping("/api/v1/students")
	public ResponseEntity<String> deleteAllStudents(){
		studentService.deleteAll();
		logger.info(applicationName + " - records of all students deleted successfully");
		return new ResponseEntity<>("All Students deleted successfully!", HttpStatus.OK);
	}
	
	@PutMapping("/api/v1/updateStudent")
	public ResponseEntity<String> updateStudent(@Valid @RequestBody Student student){
		Student updatedStudent=studentService.updateStudentDetails(student);
		logger.info(applicationName + " - Details of student with Id : " + updatedStudent.getStudentId() + " is updated successfully! ");
		return ResponseEntity.ok(" Details of the student with Id : "+ updatedStudent.getStudentId() + " is updated successfully! ");
	}
	
	@PutMapping("/api/v1/updateStudent/{studentId}")
	public ResponseEntity<String> updateStudentById(@Valid @PathVariable Long studentId,@Valid @RequestBody Student student){
		Student updatedStudent = null;
		try {
			updatedStudent = studentService.updateStudentDetailsById(studentId, student);
		} catch (StudentNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info(applicationName + " - Details of student with Id : " + updatedStudent.getFirstName() + " is updated successfully! ");
		return ResponseEntity.ok("The student with Id : "+ updatedStudent.getStudentId() + " is updated successfully! ");
	}
}
