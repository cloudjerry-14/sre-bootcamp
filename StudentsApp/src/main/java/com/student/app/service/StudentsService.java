package com.student.app.service;

import java.util.List;
import java.util.Optional;

import com.student.app.exception.StudentNotFoundException;
import com.student.app.model.Student;


public interface StudentsService {

	List<Student> getAllStudents();
	
	Optional<Student> getStudentById(Long studenId) throws StudentNotFoundException;

}
