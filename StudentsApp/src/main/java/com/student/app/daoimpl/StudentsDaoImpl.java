package com.student.app.daoimpl;

import org.springframework.data.repository.CrudRepository;

import com.student.app.model.Student;

public interface StudentsDaoImpl extends CrudRepository<Student, Long>{

}
