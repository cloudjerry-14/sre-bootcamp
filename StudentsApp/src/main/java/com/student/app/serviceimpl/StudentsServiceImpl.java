package com.student.app.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.app.daoimpl.StudentsDaoImpl;
import com.student.app.exception.StudentNotFoundException;
import com.student.app.model.Student;
import com.student.app.service.StudentsService;

@Service
public class StudentsServiceImpl implements StudentsService{

	@Autowired
	StudentsDaoImpl studentDaoImpl;
	
	@Override
	public List<Student> getAllStudents() {
		return (List<Student>) studentDaoImpl.findAll();
	}

	public Optional<Student> getStudentById(Long studentId) throws StudentNotFoundException {
		if(!studentDaoImpl.existsById(studentId))
			throw new StudentNotFoundException("Student not found with Id : "+studentId);
		return studentDaoImpl.findById(studentId);
	}

	public Iterable<Student> addAllStudent(List<Student> student) {
		return studentDaoImpl.saveAll(student);	
	}

	public Student addStudent(Student student) {
		return studentDaoImpl.save(student);
	}

	public void deleteStudentById(Long studentId) throws StudentNotFoundException {
		// TODO Auto-generated method stub
		if(!studentDaoImpl.existsById(studentId)) {
			throw new StudentNotFoundException("id: "+studentId);
		}
		studentDaoImpl.deleteById(studentId);
	}

	public void deleteAll() {
		// TODO Auto-generated method stub
		studentDaoImpl.deleteAll();
	}

	public Student updateStudentDetails(Student student) {
		// TODO Auto-generated method stub
		return studentDaoImpl.save(student);
	}

	public Student updateStudentDetailsById(Long studentId, Student student) throws StudentNotFoundException {
		// TODO Auto-generated method stub
		if(!studentDaoImpl.existsById(studentId)) {
			throw new StudentNotFoundException("id: "+studentId);
		}
		Student s =studentDaoImpl.findById(studentId).get();
	
		s.setFirstName(student.getFirstName());
		s.setLastName(student.getLastName());
		s.setAge(student.getAge());
		s.setGrade(student.getGrade());
		
		return studentDaoImpl.save(s);
	}

}
