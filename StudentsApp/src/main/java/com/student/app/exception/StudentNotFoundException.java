package com.student.app.exception;

public class StudentNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5250702912371746095L;
	
	public StudentNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentNotFoundException(String string) {
		// TODO Auto-generated constructor stub
		super(string);
	}
}
