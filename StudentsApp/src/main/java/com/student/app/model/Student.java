package com.student.app.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name="student")
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long studentId;
	
	@NotNull
	@Column(length = 20, nullable = false)
	private String firstName;
	
	@NotNull
	@Column(length = 20, nullable = false)
	private String lastName;
	
	@NotNull
	@Column(length = 3, nullable = false)
	private int age;
	
	@NotNull
	@Column(length = 1, nullable = false)
	private char grade;
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Student(long studentId, String firstName, String lastName, int age, char grade) {
		super();
		this.studentId = studentId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.grade = grade;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentID) {
		this.studentId = studentID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGrade() {
		return grade;
	}

	public void setGrade(char grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Student [studentID=" + studentId + ", firstName=" + firstName + ", lastName=" + lastName + ", age="
				+ age + ", grade=" + grade + "]";
	}
}
